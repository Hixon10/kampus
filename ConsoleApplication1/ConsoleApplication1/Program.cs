﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public class Program
    {
        static private string textPath;
        static private string queryPath;

        static void Main(string[] args)
        {
            Initialize(args);

            var text = File.ReadAllText(textPath);

            var preprocessor = new Preprocessor();
            var lines = preprocessor.SplitTextIntoLines(text);

            var queries = File.ReadAllLines(queryPath);

            var searcher = new Searcher();
            var searchManager = new SearchManager(searcher);

            var result = searchManager.Search(lines, queries);

            Console.WriteLine(result);

            Console.ReadKey();
        }

        /// <summary>
        /// Инициализация 
        /// </summary>
        /// <param name="args">переменные командной строки</param>
        private static void Initialize(string[] args)
        {
            if (args == null)
            {
                throw new NullReferenceException("args");
            }

            if (args.Length < 2)
            {
                throw new ArgumentException("Не переданы файлы");
            }

            if (!String.IsNullOrWhiteSpace(args[0]))
            {
                textPath = args[0];
            }
            else
            {
                throw new ArgumentException("Не переданы файлы");
            }

            if (!String.IsNullOrWhiteSpace(args[1]))
            {
                queryPath = args[1];
            }
            else
            {
                throw new ArgumentException("Не переданы файлы");
            }
        }
    }

    /// <summary>
    /// Управляющий поиском
    /// </summary>
    internal class SearchManager
    {
        private readonly ISearcher _searcher;
        private const int MaxNumberOfMatches = 20;

        public SearchManager(ISearcher searcher)
        {
            _searcher = searcher;
        }

        /// <summary>
        /// Создание результата для вывода на консоль
        /// </summary>
        /// <param name="lines">строки</param>
        /// <param name="queries">поисковое запросы</param>
        /// <returns>строка для вывода</returns>
        public string Search(string[] lines, string[] queries)
        {
            if (lines == null)
            {
                throw new NullReferenceException("lines");
            }

            if (queries == null)
            {
                throw new NullReferenceException("queries");
            }

            var result = new StringBuilder();

            foreach (var query in queries)
            {
                result.Append(SearchByQueryInAllLines(lines, query));
            }

            return result.ToString().TrimEnd();
        }

        /// <summary>
        /// Поиск по ключевому запросу по всем строкам
        /// </summary>
        /// <param name="lines">Строки</param>
        /// <param name="query">Запрос</param>
        /// <returns>Строки, где запрос нашёлся</returns>
        private string SearchByQueryInAllLines(string[] lines, string query)
        {
            var sb = new StringBuilder();
            int counter = 0;
            bool isNeedNewLine = false;
            
            for (int i = 0; i < lines.Length; i++)
            {
                if (!_searcher.IsContainsSearchQueryInLine(lines[i], query))
                {
                    continue;
                }

                isNeedNewLine = true;

                sb.Append(i.ToString());
                counter++;

                if (counter < MaxNumberOfMatches)
                {
                    sb.Append(", ");
                }
                else
                {
                    break;
                }
            }

            if (counter < MaxNumberOfMatches)
            {
                if (sb.Length < 2)
                {
                    sb.Length = 0;
                }
                else
                {
                    sb.Length -= 2;
                }
            }

            if (isNeedNewLine)
            {
                sb.Append(Environment.NewLine);
            }

            return sb.ToString();
        }
    } 

    internal interface ISearcher
    {
        /// <summary>
        /// Проверка на то, что поисковой запрос содержится в строке
        /// </summary>
        /// <param name="line">Строка</param>
        /// <param name="query">Поисковой запрос</param>
        /// <returns>bool</returns>
        bool IsContainsSearchQueryInLine(string line, string query);
    }

    /// <summary>
    /// Поиск поискового запроса в строке
    /// </summary>
    internal class Searcher : ISearcher
    {
        /// <summary>
        /// Проверка на то, что поисковой запрос содержится в строке
        /// </summary>
        /// <param name="line">Строка</param>
        /// <param name="query">Поисковой запрос</param>
        /// <returns>bool</returns>
        public bool IsContainsSearchQueryInLine(string line, string query)
        {
            if (line == null)
            {
                throw new NullReferenceException("line");
            }

            if (query == null)
            {
                throw new NullReferenceException("query");
            }

            var keywords = query.Split(new char[] { ' ' });

            foreach (var keyword in keywords)
            {
                var words = line.Split(new char[] { ' ' });

                if (!words.Contains(keyword, StringComparer.OrdinalIgnoreCase))
                {
                    return false;
                }

                line = RemoveKeywordFromLine(line, keyword);
            }

            return true;
        }

        /// <summary>
        /// Удаление использовавшегося ключевого слова из строки
        /// </summary>
        /// <param name="line">строка</param>
        /// <param name="keyword">ключевое слово</param>
        /// <returns>Строка без ключевого слова</returns>
        private string RemoveKeywordFromLine(string line, string keyword)
        {
            int index = line.IndexOf(keyword, StringComparison.InvariantCulture);
            string cleanString = (index < 0) ? line : line.Remove(index, keyword.Length);

            return cleanString;
        }
    }

    /// <summary>
    /// Предварительная обработка текста
    /// </summary>
    internal class Preprocessor
    {
        /// <summary>
        /// Деление текста на строки
        /// </summary>
        /// <param name="text">Текст</param>
        /// <returns>Строки</returns>
        public string[] SplitTextIntoLines(string text)
        {
            if (text == null)
            {
                throw new NullReferenceException("text");
            }

            string textWithoutSpecialCharacters = RemoveSpecialCharacters(text);
            string[] lines = textWithoutSpecialCharacters.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            return lines;
        }

        /// <summary>
        /// Удаление символов из текста, которых не может быть в ключевых запросах
        /// </summary>
        /// <param name="text">Текст</param>
        /// <returns>Очищенный от символов текст</returns>
        private string RemoveSpecialCharacters(string text)
        {
            var sb = new StringBuilder();
            foreach (var c in text.Where(IsNotSpecialCharacters))
            {
                sb.Append(c);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Проверка на то, что символ не является специальным
        /// </summary>
        /// <param name="c">символ</param>
        /// <returns>bool</returns>
        private bool IsNotSpecialCharacters(char c)
        {
            return (c <= 'Z' && c >= 'A') ||
                   (c <= 'z' && c >= 'a') ||
                   (c >= 'А' && c <= 'Я') ||
                   (c >= 'а' && c <= 'я') ||
                   (c >= '0' && c <= '9') ||
                   c == 'Ё' ||
                   c == 'ё' ||
                   c == '_' ||
                   c == ' ' ||
                   c == '\r' ||
                   c == '\n';
        }
    }
}
